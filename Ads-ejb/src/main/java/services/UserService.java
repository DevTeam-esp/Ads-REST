package services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import iservices.IUserServiceRemote;
import persistence.SimpleUser;
import persistence.User;
import utilities.GenericService;

@LocalBean
@Stateless
public class UserService extends GenericService<User> implements IUserServiceRemote{
	@PersistenceContext
	private EntityManager em;
	public UserService() {
		super(User.class);
	}

	@Override
	public String test(String first,String last) {
		// TODO Auto-generated method stub
		em.merge(new SimpleUser());
		return "ok";
	}

	
	
}
