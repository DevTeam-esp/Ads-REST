package persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import enums.AdsCategory;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;

/**
 * Entity implementation class for Entity: Ads
 *
 */
@Entity

public class Ads implements Serializable {

	   
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private int id;
	private static final long serialVersionUID = 1L;
	private String title;
	@Enumerated(EnumType.STRING)
	private AdsCategory category;
	private String description;
	@Temporal(TemporalType.DATE)
	private Date createDate;
	private boolean active;
	
	@OneToMany(mappedBy="AdsRate",cascade=CascadeType.ALL)
	private Collection<Rate> ratingCollection;
	@ManyToOne
	private SimpleUser owner;
	@ManyToMany(mappedBy="followingList")
	private Set<SimpleUser> followersList;



	public Set<SimpleUser> getFollowersList() {
		return followersList;
	}
	public void setFollowersList(Set<SimpleUser> followersList) {
		this.followersList = followersList;
	}
	public SimpleUser getOwner() {
		return owner;
	}
	public void setOwner(SimpleUser owner) {
		this.owner = owner;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public AdsCategory getCategory() {
		return category;
	}
	public void setCategory(AdsCategory category) {
		this.category = category;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Ads() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
   
}
