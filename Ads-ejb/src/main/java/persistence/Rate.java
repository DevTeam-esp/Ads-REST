package persistence;

import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Rate")
public class Rate {

	@EmbeddedId
	private RateID id=new RateID();

	@ManyToOne
	@JoinColumn(name="user_id",insertable=false,updatable=false)
	private User simpleUserRate;
	
	@ManyToOne
	@JoinColumn(name="ads_id",insertable=false,updatable=false)
	private Ads AdsRate;
	
	
	private int score;
	
	@Temporal(TemporalType.DATE)
	private Date createdAt;
	
	
}
