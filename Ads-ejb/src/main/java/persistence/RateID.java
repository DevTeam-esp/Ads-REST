package persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class RateID implements Serializable{

		@Column(name="user_id")
		private int userId;
		
		@Column(name="ads_id")
		private int adsId;
		
	
		public RateID(){}
		
		public RateID(int userId,int adsId){
			this.userId=userId;
			this.adsId=adsId;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + adsId;
			result = prime * result + userId;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			RateID other = (RateID) obj;
			if (adsId != other.adsId)
				return false;
			if (userId != other.userId)
				return false;
			return true;
		}
		
		
}
