package persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: SimpleUser
 *
 */
@Entity
@DiscriminatorValue("SimpleUser")
public class SimpleUser extends User implements Serializable {

	@ManyToMany
    private Set<Ads>followingList;
	private boolean active;
	private int tel;
	
	@OneToMany(mappedBy="simpleUserRate",cascade=CascadeType.ALL)
	private Collection<Rate> ratingCollection;
	
	

	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public int getTel() {
		return tel;
	}
	public void setTel(int tel) {
		this.tel = tel;
	}
	public Set<Ads> getFollowingList() {
		return followingList;
	}
	public void setFollowingList(Set<Ads> followingList) {
		this.followingList = followingList;
	}
	public SimpleUser() {
		super();
	}   

   
}
