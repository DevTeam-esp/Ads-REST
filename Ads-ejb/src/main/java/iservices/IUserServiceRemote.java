package iservices;

import java.io.Serializable;

import javax.ejb.Remote;
import javax.persistence.EntityExistsException;
import javax.persistence.PersistenceException;
import javax.persistence.TransactionRequiredException;

import persistence.User;
import utilities.IGenericService;

@Remote
public interface IUserServiceRemote extends IGenericService<User>{

	String test(String u,String t);


}