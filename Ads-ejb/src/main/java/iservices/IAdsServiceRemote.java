package iservices;

import javax.ejb.Remote;

import persistence.Ads;
import utilities.IGenericService;

@Remote
public interface IAdsServiceRemote extends IGenericService<Ads> {
    void khra();
}
